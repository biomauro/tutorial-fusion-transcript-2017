#!/bin/bash
source /opt/conda/bin/activate
export JAFFA_REF_BASE=/nfs/fusion/JAFFA/

cd ~/fusion/tutorial-fusion-transcript-2017/Result/JAFFA/
#check  command options
#jaffa-[tab]
mkdir -p BT4
cd BT4/
echo "Prepare Assemble fusion of BT474  Breast lines cell"
 
jaffa-assembly ~/fusion/tutorial-fusion-transcript-2017/RAW/BT474-demo_*.fastq.gz 
cd ..
echo "##############################################################"
echo "Prepare Direct fusion f BT474  Breast lines cell"
mkdir -p Direct

cd  Direct/
jaffa-Direct ~/fusion/tutorial-fusion-transcript-2017/RAW/BT474-demo_*.fastq.gz 
cd ..
echo "Prepare Hybrid fusion of BT474  Breast lines cell"
mkdir Hybrid
cd Hybrid
jaffa-hybrid ~/fusion/tutorial-fusion-transcript-2017/RAW/BT474-demo_*.fastq.gz 
cd ..
echo "##############################################################"
echo "Create MCF7 assembly"
mkdir -p MCF7
cd MCF7/

jaffa-assembly ~/fusion/tutorial-fusion-transcript-2017/RAW/MCF7-demo_*.fastq.gz

cd ..
rm -rf BT4/ MCF7/ Hybrid/ Direct/

echo "##############################################################"
mkdir ALL

jaffa-assembly ~/fusion/tutorial-fusion-transcript-2017/RAW/MCF7-demo_*.fastq.gz ~/fusion/tutorial-fusion-transcript-2017/RAW/BT474-demo_*.fastq.gz




