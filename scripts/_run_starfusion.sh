#!/bin/bash

export TUTORIAL=/home/maurizio/Desktop/fusions/
export Starfusion=/opt/STAR-Fusion-v1.0.0/STAR-Fusion
export REF_STAR=/home/maurizio/Desktop/fusions/REF/star-fusion/GRCh37_gencode_v19_CTAT_lib_July192017/
left_fq="reads_1.fq.gz"
right_fq="reads_2.fq.gz"


mkdir -p ALL
cd ALL

${starfusion}  --genome_lib_dir ${REF_STAR}  --left_fq ${TUTORIAL}/RAW/reads_1.fastq.gz --right_fq ${TUTORIAL}/RAW/reads_2.fastq.gz  -O star_fusion


cd ..
mkdir INSPECT
cd INSPECT
${starfusion}  --genome_lib_dir ${REF_STAR}  --left_fq ${TUTORIAL}/RAW/reads_1.fastq.gz --right_fq ${TUTORIAL}/RAW/reads_2.fastq.gz  -O star_fusion_inpect --FusionInspector inspect 



 




