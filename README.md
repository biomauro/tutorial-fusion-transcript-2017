## INTRODUCTION

Here we have some scripts and data for run a tutorial for detect Fusion using RNA-seq data.
[maurizio@localhost tutorial-fusion-transcript-2017]$ tree 
.
├── RAW
│   ├── BT474-demo_1.fastq.gz
│   ├── BT474-demo_2.fastq.gz
│   ├── MCF7-demo_1.fastq.gz
│   ├── MCF7-demo_2.fastq.gz
│   ├── R1_100.fq.gz
│   ├── R2_100.fq.gz
│   ├── reads_1.fastq.gz
│   ├── reads_2.fastq.gz
│   ├── reads_50x_1.fastq.gz -> reads_50x_R1.fq.gz
│   ├── reads_50x_2.fastq.gz -> reads_50x_R2.fq.gz
│   ├── reads_50x_R1.fq.gz
│   └── reads_50x_R2.fq.gz
├── README.md
├── Results
│   ├── Defuse
│   ├── HCC1395
│   │   ├── defuse.tar.gz
│   │   └── starfusion.tar.gz
│   ├── JAFFA
│   │   ├── BT474_MCF7.tar.gz
│   │   └── Simulazione_50x.tar.gz
│   └── STAR
│       ├── Sim_50x_small.tar.gz
│       └── test_kickstart_small.tar.gz
└── scripts
    ├── _run_starfusion.sh
    └── _script.jaffa.sh



On the RAW you have all the data we need for run this tutorial.
Result folder contain the resulst of the execise. 
HCC1395 sample contain the result of scipt run on this sample for detect fusion.

